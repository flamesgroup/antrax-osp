# antrax-osp

Assembly `rpm` and `deb` packages of ANTRAX system.

## Overview

**antrax-osp** is using [Gradle](https://en.wikipedia.org/wiki/Gradle) build system with [gradle-ospackage-plugin](https://github.com/nebula-plugins/gradle-ospackage-plugin) to build `rpm` and `deb`
packages of ANTRAX components, confrst and tools.

`rpm` packages build only for is `x86_64` architecture.
`deb` packages build for `amd64` and `armhf` architecture.

## Structure

* _commons_ - assembly of commons package
  * _src_ - directory contains common resources and scripts for running all components of ANTRAX
  * _build.gradle_
* _confrst_ - assembly of confrst package
  * _src_ - empty directory for correct building confrst project
  * _build.gradle_
* _gui-client-webstart_ - assembly of gui-client-webstart package
  * _src_ - directory contains resources and scripts for running gui-client-webstart component
  * _build.gradle_ - build configuration for Gradle
* _scriptlet_ - scripts of corresponding projects
* _server_ - common project for all server components of ANTRAX
  * _control-server_ - assembly of control-server packages
    * _src_ - directory contains resources and scripts for running control-server component
    * _build.gradle_
  * _sim-server_ - assembly of sim-server packages
    * _src_ - directory contains resources and scripts for running sim-server component
    * _build.gradle_
  * _voice-server_ - assembly of voice-server packages
    * _src_ - directory contains resources and scripts for running voice-server component
    * _build.gradle_
  * _build.gradle_
* _tools_ - assembly of tools package
  * _src_ - directory contains scripts for running tools of ANTRAX and devices-protocol
  * _build.gradle_
* _build.gradle_ - build configuration for Gradle
* _gradle.properties_ - file contains group name and version of project
* _osp.build_ - bash script to build packages
* _settings.gradle_ - configuration of included sub project

## Install

### Environment
* [Oracle JDK 8](http://www.oracle.com/technetwork/java/javase/overview/index.html) or higher
* [Groovy 2](http://www.groovy-lang.org/syntax.html)
* [Gradle 3](https://gradle.org)
* [Maven](https://maven.apache.org)
* [createrepo](http://createrepo.baseurl.org)

### Artifacts

All required artifacts of ANTRAX and confrst must be installed to local maven repository.
Also remote artifact repository manager such as [Artifactory](https://www.jfrog.com/artifactory/) could be used with proper Gradle and Maven configuration.

#### Gradle configuration

If remote artifact repository manager is used, then next properties must be defined in Gradle configuration (usually `$USER_HOME/.gradle/gradle.properties`):
* `maven_repo_url` - url of remote artifact repository manager
* `maven_repo_username` - username to assess remote artifact repository manager
* `maven_repo_password` - password to assess remote artifact repository manager

Examples:
```
maven_repo_url=https://devzone.flamesgroup.com/artifactory/libs-snapshot-local
maven_repo_username=developer
maven_repo_password=qweqwe
```

#### Maven configuration

See [Settings Reference](https://maven.apache.org/settings.html) for more details.

## Usage

To run `rpm` and `deb` packages assembly process next command must be executed with correct parameters:
```
./osp.build <antrax artifacts version> <destination directory path to packeges>
```

Examples:
```
./osp.build 1.15.0-SNAPSHOT ~/antrax-packages
```

## Questions

If you have any questions about **antrax-osp**, feel free to create issue with it.
