#!/usr/bin/python2
import os
import jprop
import re

from time import time

PROPERTY_PATH = "/opt/antrax/voice-server/etc/voice-server.properties"

def get_lifetime_s(expire_time_string):

    hours_pattern = re.compile(".*(?P<hours>\d{1,2})[hH].*")
    days_pattern = re.compile(".*(?P<days>\d{1,2})[dD].*")
    months_pattern = re.compile(".*(?P<months>\d{1,2})[mM].*")
    years_pattern = re.compile(".*(?P<years>\d{1,2})[yY].*")

    hours = 0
    days = 0
    months = 0
    years = 0

    hours_match = hours_pattern.match(expire_time_string)
    if hours_match:
        hours = int(hours_match.group("hours"))

    days_match = days_pattern.match(expire_time_string)
    if days_match:
        days = int(days_match.group("days"))

    months_match = months_pattern.match(expire_time_string)
    if months_match:
        months = int(months_match.group("months"))

    years_match = years_pattern.match(expire_time_string)
    if years_match:
        years = int(years_match.group("years"))

    return (years * 365 * 24 + months * 30 * 24 + days * 24 + hours) * 3600


def cleanup_files(path, expire_time, file_extension):
    lifetime = get_lifetime_s(expire_time)
    for subdir, dirs, files in os.walk(path):
        for file in files:
            filepath = path + file
            if file.endswith(file_extension):
                creation_date = int(os.path.getmtime(filepath))
                if int(time()) - creation_date > lifetime:
                    print("Removing file: " + file)
                    os.remove(filepath)


if __name__ == '__main__':

    properties = jprop.Properties()
    properties.load(open(PROPERTY_PATH, "r"))

    audiocapture_path = properties.getProperty("voice-server.audio.capture.path")
    audiocapture_expire_time = properties.getProperty("voice-server.audio.capture.expire.time")
    if audiocapture_path and audiocapture_expire_time:
        cleanup_files(audiocapture_path, audiocapture_expire_time, ".pcm")

    trace_path = properties.getProperty("voice-server.telit.trace.path")
    trace_expire_time = properties.getProperty("voice-server.telit.trace.expire.time")
    if trace_path and trace_expire_time:
        cleanup_files(trace_path, trace_expire_time, ".bin")
