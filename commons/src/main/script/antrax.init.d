#!/bin/bash

NAME="$1"
COMMAND="$2"

BIN_DIR=./bin
ETC_DIR=./etc
LOCK_DIR=./lock
LOG_DIR=./log
LOCK_FILE=$LOCK_DIR/$NAME.pid
LOG_ERR="$( pwd )"/log/$NAME.std"(err|out)"

RETVAL=0

failure() {
  echo -ne "\t\t["
  echo -ne "\033[1;31mFAILED\033[0m"
  echo -n "]"
}

success() {
  echo -ne "\t\t["
  echo -ne "\033[0;32mOK\033[0m"
  echo -n "]"
}

function echo_red() {
  echo -ne "\e[1;31m$1\e[0m"
}

start() {
  PIDS=""
  COUNT=0
  for PID in `ps -ef | awk '$8=="'"$NAME"'" {print $2}'`; do
    PIDS+="$PID "
    ((COUNT+=1))
  done
  if [ $COUNT -eq 2 ]; then
    echo_red "$NAME has already started with pid "`cat $LOCK_FILE`
    echo
    exit 1
  fi
  if [ $COUNT -eq 1 ]; then
    echo_red "$NAME is restarting"
    echo
    exit 1
  fi
  if [ -f $LOCK_FILE ]; then
        rm $LOCK_FILE
  fi

  for LOG in `ls $LOG_DIR/*.std??? 2> /dev/null`; do
    LOG_BASENAME=`basename $LOG`
    LOG_ZIP="$LOG_DIR/back/$LOG_BASENAME.$(date +%Y-%m-%d.%H:%M:%S).zip"
    zip -j $LOG_ZIP $LOG &> /dev/null
    rm -f $LOG
    chown antrax:antrax $LOG_ZIP &> /dev/null
  done

  echo -n $"Starting $NAME: "
  $BIN_DIR/$NAME > /dev/null
  RETVAL=$?

  if [ $RETVAL -eq 0 ]; then
    success
  else
    echo_red "Can't start. For details see $LOG_ERR"
    failure
  fi
  echo
}

stop() {
  PIDS=""
  COUNT=0
  for PID in `ps -ef | awk '$8=="'"$NAME"'" {print $2}'`; do
    PIDS+="$PID "
    COUNT+=1
  done
  if [ -z "$PIDS" ]; then
    echo_red "$NAME is not started"
    echo
    exit 0
  else
    if [ $COUNT -eq 1 ]; then
     echo_red "$NAME is restarting, try stop later or kill now"
     echo
     exit 0
    fi
  fi
    echo -n $"Stopping $NAME: "
    $BIN_DIR/$NAME -stop  > /dev/null
    RETVAL=$?
    if [ $RETVAL -eq 0 ]; then
      rm -f $LOCK_FILE
      success
    else
      echo_red "Can't stop. For details see $LOG_ERR"
      failure
    fi
  echo
}

terminate() {
  PIDS=""
  for PID in `ps -ef | awk '$8=="'"$NAME"'" {print $2}'`; do
    PIDS+="$PID "
  done
  if [ -z "$PIDS" ]; then
    echo_red "$NAME is not started"
    echo
    exit 1
  fi
  echo "kill $NAME with pids $PIDS"
  kill -9 $PIDS
  exit 0
}

restart() {
  stop
  if [ $RETVAL -ne 0 ]; then
    return  $RETVAL
  fi
  start
}

other() {
  if [ ! -f $LOCK_FILE ]; then
    echo_red "$NAME is not started"
    echo
    exit 1
  fi
  . $ETC_DIR/$NAME.script.properties
  echo $COMMAND | nc localhost $daemonExtPort
}

if [ $# = 1 ]; then
. $ETC_DIR/$NAME.script.properties
  echo "Usage: {start|restart|stop|kill}"
  echo "*" | nc localhost $daemonExtPort 2> /dev/null
  exit 0
fi

case $COMMAND in
  start)
    start
    ;;
  stop)
    stop
    ;;
  restart)
    restart
    ;;
  kill)
    terminate
    ;;
  *)
    other
esac

exit $RETVAL
